from psychopy import core, data, event, visual
from number_stim import NumberStim

class CompareNumbers:
    title = 'Compare Numbers'
    description = ('Press left arrow if the left side is larger, '
                   'press right arrow if the right side is larger.')

    def __init__(self, win, path):
        self.win = win
        self.path = path
        self.clock = core.Clock()
        trial_list = [{
            'left_number': i,
            'right_number': i + [-2, -1, 1, 2][i % 4],
            'left_extra': [-1, 0, i//4, -1, 0, i//3, -1, 0, i//2][i % 9],
            'right_extra': [-1, 0, i//4, -1, 0, i//3, -1, 0, i//2][(i + 1) % 9]
            } for i in range(10, 20)]
        self.trials = data.TrialHandler(trial_list, nReps=1, seed=0)
        self.sep = visual.Rect(win,
                               width=0,
                               height=2,
                               lineColor=(1, 1, 1),
                               lineWidth=1)
        self.left = NumberStim(win, pos=[-0.5, 0])
        self.right = NumberStim(win, pos=[0.5, 0])

    def run(self):
        for trial in self.trials:
            self.left.value = trial['left_number']
            self.right.value = trial['right_number']
            self.left.extra = trial['left_extra']
            self.right.extra = trial['right_extra']
            self.left.update()
            self.right.update()

            self.clock.reset()
            self.sep.draw()
            self.left.draw()
            self.right.draw()
            self.win.flip()

            keys = event.waitKeys(keyList=['left', 'right'],
                                  timeStamped=self.clock)
            self.trials.data.add('right_key', keys[0][0] == 'right')
            self.trials.data.add('time', keys[0][1])

        self.trials.saveAsWideText(self.path)
