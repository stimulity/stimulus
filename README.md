# stimulus

## Installation

1. Clone the repo
```sh
git clone https://gitlab.com/stimulity/stimulus.git
```
2. Install psychopy
```sh
pip install --user psychopy
```
3. Run it
```sh
python main.py
```
