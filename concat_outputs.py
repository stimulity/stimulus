from io import StringIO
import pandas as pd

path_prefix = '../../stimulus/'
tests = ['2hz', '5hz', '10hz']
tasks = ['stopped_or_not', 'click_target', 'compare_numbers']

def get(filename):
    with open(path_prefix + filename + '.tsv') as f:
        return f.read().split('\ufeff')

def read(filename, test):
    return pd.read_csv(StringIO(get(filename)[test]), sep='\t')

def data(test):
    dfs = []
    for i in range(3):
        for j in range(1, 4):
            df = read(tests[i] + str(j), test)
            df.insert(0, 'ExpNumber', j)
            df.insert(0, 'TypeNumber', i)
            dfs.append(df)
    return pd.concat(dfs, ignore_index=True)

for i, name in enumerate(tasks):
    df = data(i + 1)
    df.to_csv(path_prefix + name + '.csv', index=False)
