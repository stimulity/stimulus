import os
from psychopy import event, gui, visual
from stopped_or_not import StoppedOrNot
from click_target import ClickTarget
from compare_numbers import CompareNumbers

def show_overlay(win, title, description):
    title_stim = visual.TextStim(win,
                                 pos=[0, 0.5],
                                 height=0.2,
                                 text=title)
    desc_stim = visual.TextStim(win,
                                pos=[0, -0.5],
                                height=0.1,
                                text=description)
    title_stim.draw()
    desc_stim.draw()
    win.flip()
    event.waitKeys()

def main():
    title = 'Experiment'
    description = 'Press any key to start.'
    end_description = 'Thanks for your participation in this experiment!'

    tasks = [
        StoppedOrNot,
        ClickTarget,
        CompareNumbers,
    ]

    win = visual.Window(units='norm')
    win.aspect_ratio = win.size[0] / win.size[1]

    folder = os.path.join(os.path.expanduser('~'), 'stimulus')
    os.makedirs(folder, exist_ok=True)
    path = gui.fileSaveDlg(initFilePath=folder, allowed='TSV files (*.tsv)')
    if not path:
        return

    initialized_tasks = [task(win, path) for task in tasks]
    show_overlay(win, title, description)
    for task in initialized_tasks:
        show_overlay(win, task.title, task.description)
        task.run()
    show_overlay(win, title, end_description)

if __name__ == '__main__':
    main()
