from itertools import count
from psychopy import core, data, event, visual

class ClickTarget:
    title = 'Click Target'
    description = 'Press any key when the box reaches the red target.'

    def __init__(self, win, path):
        self.win = win
        self.path = path
        self.clock = core.Clock()
        trial_list = [{'duration': i / 10} for i in range(10, 30, 2)]
        self.trials = data.TrialHandler(trial_list, nReps=1, seed=0)
        self.box = visual.Rect(win,
                               width=0.2,
                               height=0.2 * win.aspect_ratio,
                               fillColor=(1, 1, 1),
                               lineColor=None)
        self.wall = visual.Rect(win,
                                width=1.2,
                                height=2,
                                pos=[0.4, 0],
                                fillColor=(1, 1, 1),
                                lineColor=None)
        self.target = visual.Rect(win,
                                  width=0.2,
                                  height=0.2 * win.aspect_ratio,
                                  pos=[0.5, 0],
                                  lineColor=(1, 0, 0),
                                  lineWidth=2)

    def run(self):
        for trial in self.trials:
            duration = trial['duration']

            self.clock.reset()
            for frame in count():
                progress = frame / 60 / duration
                progress = min(progress, 1)
                self.box.pos = [progress * 1.5 - 1, 0]

                self.box.draw()
                self.wall.draw()
                self.target.draw()
                self.win.flip()

                keys = event.getKeys(timeStamped=self.clock)
                if keys:
                    self.trials.data.add('time', keys[0][1])
                    break

        self.trials.saveAsWideText(self.path)
