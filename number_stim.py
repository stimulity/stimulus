import numpy as np
from psychopy import visual

class NumberStim:
    def __init__(self, win, pos):
        size = 0.02

        points = [[0, 0]] + [[
            size * 4 * r * np.cos(np.radians(a)),
            size * 4 * win.aspect_ratio * r * np.sin(np.radians(a))
            ] for r in range(1, 3) for a in range(0, 360, 60 // r)]

        np.random.RandomState(0).shuffle(points)

        self.circles = [visual.Circle(win,
                                      radius=[size, size * win.aspect_ratio],
                                      pos=[pos[0] + point[0],
                                           pos[1] + point[1]],
                                      fillColor=(1, 1, 1),
                                      lineColor=None) for point in points]

        self.text = visual.TextStim(win, pos=pos, height=0.2)

        self.value = 0
        self.extra = 0

    def draw(self):
        if self.extra == -1:
            for circle in self.circles[:self.value]:
                circle.draw()
        else:
            self.text.draw()

    def update(self):
        self.text.text = (str(self.value) if self.extra == 0 else
                          str(self.extra) + '+' + str(self.value - self.extra))
