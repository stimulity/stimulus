from itertools import count
from psychopy import core, data, event, visual

class StoppedOrNot:
    title = 'Stopped or Not'
    description = ('Press left arrow if the box stopped below the wall, '
                   'press right arrow if the box not stopped.')

    def __init__(self, win, path):
        self.win = win
        self.path = path
        self.clock = core.Clock()
        trial_list = [{
            'duration': i / 10,
            'stopped': i / 2 % 2 == 1,
            'reverse': i / 2 % 4 < 2} for i in range(10, 30, 2)]
        self.trials = data.TrialHandler(trial_list, nReps=1, seed=0)
        self.box = visual.Rect(win,
                               width=0.2,
                               height=0.2 * win.aspect_ratio,
                               fillColor=(1, 1, 1),
                               lineColor=None)
        self.wall = visual.Rect(win,
                                width=0.4,
                                height=2,
                                fillColor=(1, 1, 1),
                                lineColor=None)

    def run(self):
        for trial in self.trials:
            duration = trial['duration']
            reverse = trial['reverse']
            stopped = trial['stopped']

            self.clock.reset()
            for frame in count():
                progress = frame / 60 / duration
                if progress > 1:
                    progress = 0.5
                if reverse:
                    progress = 1 - progress
                if stopped:
                    if progress < 0.4:
                        progress = progress / 0.4 * 0.5
                    elif progress < 0.6:
                        progress = 0.5
                    else:
                        progress = (progress - 0.6) / 0.4 * 0.5 + 0.5
                self.box.pos = [progress * 1.2 - 0.6, 0]

                self.box.draw()
                self.wall.draw()
                self.win.flip()

                keys = event.getKeys(keyList=['left', 'right'],
                                     timeStamped=self.clock)
                if keys:
                    self.trials.data.add('right_key', keys[0][0] == 'right')
                    self.trials.data.add('time', keys[0][1])
                    break

        self.trials.saveAsWideText(self.path)
